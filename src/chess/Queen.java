package chess;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Queen extends Piece 
{
	private Rook fakeRook;
	private Bishop fakeBishop;
	
	public Queen(int x, int y, int s)
	{
		super(x, y, s);
		fakeRook = new Rook(x,y,s);
		fakeBishop = new Bishop(x,y,s);
		
		setImage(new ImageIcon((s == WHITE) ? "WQueen.png" : "BQueen.png"));
	}

	public void move(int x, int y)
	{
		fakeRook.tempMove(x, y);
		fakeBishop.tempMove(x, y);
		super.move(x,y);
	}
	
	public boolean canAttack(int x, int y)
	{
		if(fakeRook.canAttack(x, y)) return true;
		else return fakeBishop.canAttack(x, y);
	}

	public ArrayList<BoardSquare> getMoves()
	{
		ArrayList<BoardSquare> squares = new ArrayList<BoardSquare>();
		
		squares.addAll(fakeRook.getMoves());
		squares.addAll(fakeBishop.getMoves());
		
		return squares;
	}
	
	public String toString()
	{
		if(getSide() == WHITE) return "WQueen " + getX() + " " + getY();
		else return "BQueen " + getX() + " " + getY();
	}
}