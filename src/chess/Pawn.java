package chess;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Pawn extends Piece
{
	private static final int WHITE_START = 6, BLACK_START = 1;
	private int direction;
	
	public Pawn(int x, int y, int s)
	{
		super(x, y, s);
		direction = (getSide() == WHITE) ? 1 : -1;
		setImage(new ImageIcon((s == WHITE) ? "WPawn.png" : "BPawn.png"));
	}

	public void move(int x, int y)
	{
		if(getX() != x) enPasant(x, y);
		if(Math.abs(getY() - y) == 2) Chess.setLastPawn(this);
		
		super.move(x,y);

		if(getY() < BLACK_START || getY() > WHITE_START) Chess.promote(this);
	}
	
	private void enPasant(int x, int y)
	{
		if(getBoard().getSquare(x,y).hasPiece()) return;
		
		Chess.capture(getBoard().getSquare(x,y+direction).getPiece());
		getBoard().clear(x,y+direction);
	}

	public boolean canMove(int x, int y)
	{
		Piece enPasantLeft = (getBoard().getSquare(getX() - 1, getY()) != null) ? getBoard().getSquare(getX() - 1, getY()).getPiece() : null;
		Piece enPasantRight = (getBoard().getSquare(getX() + 1, getY()) != null) ? getBoard().getSquare(getX() + 1, getY()).getPiece() : null;
		int start = (getSide() == WHITE) ? WHITE_START : BLACK_START;
		boolean canMoveHere = false;
		
		BoardSquare left = getBoard().getSquare(getX() - 1, getY() - direction);
		BoardSquare right = getBoard().getSquare(getX() + 1, getY() - direction);

		if(x == getX() && getY() - y == direction && !getBoard().getSquare(x, y).hasPiece()) canMoveHere = true;
		else if(x == getX() && getY() == start && getY() - y == 2 * direction && !getBoard().getSquare(x, y + direction).hasPiece() && !getBoard().getSquare(x, y).hasPiece()) canMoveHere = true;
		else if(x == getX() - 1 && getY() - y == direction && left != null && left.hasPiece() && left.getPiece().getSide() != getSide()) canMoveHere = true;
		else if(x == getX() - 1 && getY() - y == direction && enPasantLeft != null && enPasantLeft == Chess.getLastPawn()) canMoveHere = true;
		else if(x == getX() + 1 && getY() - y == direction && right != null && right.hasPiece() && right.getPiece().getSide() != getSide()) canMoveHere = true;
		else if(x == getX() + 1 && getY() - y == direction && enPasantRight != null && enPasantRight == Chess.getLastPawn()) canMoveHere = true;

		return canMoveHere && Chess.testToBlockCheck(x, y, this);
	}

	public boolean canAttack(int x, int y)
	{
		if((x == getX() + 1 || x == getX() - 1) && getY() - y == direction) return true;

		return false;
	}

	public ArrayList<BoardSquare> getMoves()
	{
		ArrayList<BoardSquare> squares = new ArrayList<BoardSquare>();
		
		if(canMove(getX(), getY() - direction)) squares.add(getBoard().getSquare(getX(), getY() - direction));
		if(canMove(getX(), getY() - 2*direction)) squares.add(getBoard().getSquare(getX(), getY() - 2*direction));
		if(canMove(getX() - 1, getY() - direction)) squares.add(getBoard().getSquare(getX() - 1, getY() - direction));
		if(canMove(getX() + 1, getY() - direction)) squares.add(getBoard().getSquare(getX() + 1, getY() - direction));

		return testForCheck(squares);
	}
	
	public String toString()
	{
		if(getSide() == WHITE) return "WPawn " + getX() + " " + getY();
		else return "BPawn " + getX() + " " + getY();
	}
}