package chess;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Knight extends Piece {

	public Knight(int x, int y, int s) 
	{
		super(x, y, s);
		setImage(new ImageIcon((s == WHITE) ? "WKnight.png" : "BKnight.png"));
	}

	public boolean canAttack(int x, int y)
	{
		if(Math.abs(getX() - x) == 2 && Math.abs(getY() - y) == 1) return true;
		else if(Math.abs(getX() - x) == 1 && Math.abs(getY() - y) == 2) return true;
		else return false;
	}

	public ArrayList<BoardSquare> getMoves() 
	{
		ArrayList<BoardSquare> squares = new ArrayList<BoardSquare>();
		BoardSquare s;
		
		for(int a=-2;a<3;a++)
			if(a != 0)
				for(int b=-2;b<3;b++)
					if(b != 0 && Math.abs(a) != Math.abs(b))
					{
						s = getBoard().getSquare(getX() + a, getY() + b);
						if(s != null && (!s.hasPiece() || s.getPiece().getSide() != getSide())) squares.add(s);
					}
		
		return testForCheck(squares);
	}
	
	public String toString()
	{
		if(getSide() == WHITE) return "WKnight " + getX() + " " + getY();
		else return "BKnight " + getX() + " " + getY();
	}
}