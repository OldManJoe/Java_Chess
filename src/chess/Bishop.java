package chess;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Bishop extends Piece 
{
	public Bishop(int x, int y, int s) 
	{
		super(x, y, s);
		setImage(new ImageIcon((s == WHITE) ? "WBishop.png" : "BBishop.png"));
	}

	public boolean canAttack(int x, int y)
	{
		BoardSquare s = getBoard().getSquare(getX(), getY());

		if(Math.abs(getX() - x) != Math.abs(getY() - y)) return false; 
		
		if(x < getX())
		{
			if(y > getY()) //look LEFT_DOWN
				do 
				{
					s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.LEFT_DOWN);
					if(s != null && (s.getSquareX() == x && s.getSquareY() == y)) return true;
				}while (s != null && s.getPiece() == null);
			else if(y < getY()) //look LEFT_UP
				do
				{
					s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.LEFT_UP);
					if(s != null && (s.getSquareX() == x && s.getSquareY() == y)) return true;
				}while (s != null && s.getPiece() == null);
		}
		else if(x > getX())
		{
			if(y > getY()) //look RIGHT_DOWN
				do
				{
					s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.RIGHT_DOWN);
					if(s != null && (s.getSquareX() == x && s.getSquareY() == y)) return true;
				}while (s != null && s.getPiece() == null);
			else if(y < getY()) //look RIGHT_UP
			do 
			{
				s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.RIGHT_UP);
				if(s != null && (s.getSquareX() == x && s.getSquareY() == y)) return true;
			}while (s != null && s.getPiece() == null);
		}

		return false;
	}

	public ArrayList<BoardSquare> getMoves()
	{
		ArrayList<BoardSquare> squares = new ArrayList<BoardSquare>();
		BoardSquare s = getBoard().getSquare(getX(), getY());

		//look left_up
		do
		{
			s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.LEFT_UP);
			if(s != null && (!s.hasPiece() || s.getPiece().getSide() != getSide())) squares.add(s);
		}while(s != null && s.getPiece() == null);
		
		s = getBoard().getSquare(getX(), getY());
		//look right_up
		do
		{
			s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.RIGHT_UP);
			if(s != null && (!s.hasPiece() || s.getPiece().getSide() != getSide())) squares.add(s);
		}while(s != null && s.getPiece() == null);
		
		s = getBoard().getSquare(getX(), getY());
		//look left_down
		do
		{
			s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.LEFT_DOWN);
			if(s != null && (!s.hasPiece() || s.getPiece().getSide() != getSide())) squares.add(s);
		}while(s != null && s.getPiece() == null);
		
		s = getBoard().getSquare(getX(), getY());
		//look right_down
		do
		{
			s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.RIGHT_DOWN);
			if(s != null && (!s.hasPiece() || s.getPiece().getSide() != getSide())) squares.add(s);
		}while(s != null && s.getPiece() == null);
		
		return testForCheck(squares);
	}
	
	public String toString()
	{
		if(getSide() == WHITE) return "WBishop";
		else return "BBishop";
	}
}