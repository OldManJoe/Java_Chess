package chess;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class Rook extends Piece
{
	private boolean canCastle = true;
	
	public Rook(int x, int y, int s)
	{
		super(x,y,s);
		setImage(new ImageIcon((s == WHITE) ? "WRook.png" : "BRook.png"));
	}

	public void move(int x, int y)
	{
		super.move(x,y);
		canCastle = false;
	}
	
	public boolean canAttack(int x, int y)
	{
		BoardSquare s = getBoard().getSquare(getX(), getY());

		if(x == getX())
		{
			if(y > getY()) //look down
				do 
				{
					s = getBoard().getAdjacentSquare(s, Board.Direction.DOWN);
					if(s != null && s.getSquareX() == x && s.getSquareY() == y) return true;
				}while (s != null && s.getPiece() == null);
			else if(y < getY()) //look up
				do
				{
					s = getBoard().getAdjacentSquare(s, Board.Direction.UP);
					if(s != null && s.getSquareX() == x && s.getSquareY() == y) return true;
				}while (s != null && s.getPiece() == null);
		}
		else if(y == getY())
		{
			if(x > getX()) //look right
				do
				{
					s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.RIGHT);
					if(s != null && s.getSquareX() == x && s.getSquareY() == y) return true;
				}while (s != null && s.getPiece() == null);
			else if(x < getX()) //look left
				do 
				{
					s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.LEFT);
					if(s != null && s.getSquareX() == x && s.getSquareY() == y) return true;
				}while (s != null && s.getPiece() == null);
		}

		return false;
	}

	public ArrayList<BoardSquare> getMoves()
	{
		ArrayList<BoardSquare> squares = new ArrayList<BoardSquare>();
		BoardSquare s = getBoard().getSquare(getX(), getY());

		//look down
		do
		{
			s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.DOWN);
			if(s != null && (!s.hasPiece() || s.getPiece().getSide() != getSide())) squares.add(s);
		}while(s != null && s.getPiece() == null);
		
		s = getBoard().getSquare(getX(), getY());
		//look up
		do
		{
			s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.UP);
			if(s != null && (!s.hasPiece() || s.getPiece().getSide() != getSide())) squares.add(s);
		}while(s != null && s.getPiece() == null);
		
		s = getBoard().getSquare(getX(), getY());
		//look left
		do
		{
			s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.LEFT);
			if(s != null && (!s.hasPiece() || s.getPiece().getSide() != getSide())) squares.add(s);
		}while(s != null && s.getPiece() == null);
		
		s = getBoard().getSquare(getX(), getY());
		//look right
		do
		{
			s = getBoard().getAdjacentSquare(s.getSquareX(), s.getSquareY(), Board.Direction.RIGHT);
			if(s != null && (!s.hasPiece() || s.getPiece().getSide() != getSide())) squares.add(s);
		}while(s != null && s.getPiece() == null);
		
		return testForCheck(squares);
	}
	
	public boolean canCastle() {return canCastle;}
	
	public String toString()
	{
		if(getSide() == WHITE) return "WRook " + getX() + " " + getY();
		else return "BRook " + getX() + " " + getY();
	}
}