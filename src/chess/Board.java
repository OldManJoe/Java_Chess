package chess;

import java.awt.event.MouseListener;
import java.util.ArrayList;

public class Board
{
	private BoardSquare[][] squares = new BoardSquare[8][8];
	private BoardSquare selected = null;

	public Board(MouseListener ml)
	{
		for(int x=0;x<8;x++)
			for(int y=0;y<8;y++)
				squares[x][y] = new BoardSquare(x,y,ml);
	}

	public BoardSquare[][] displaySquares() {return squares;}

	public void clear()
	{
		for(BoardSquare[] sBox : squares)
			for(BoardSquare s : sBox)
				s.setPiece(null);
	}
	public void clear(int x, int y) {squares[x][y].setPiece(null);}
	public void tempClear(int x, int y) {squares[x][y].tempPiece(null);}
	
	public BoardSquare getSquare(int x, int y) 
	{
		try{return squares[x][y];}
		catch(ArrayIndexOutOfBoundsException e) {return null;}
	}

	public BoardSquare getAdjacentSquare(BoardSquare s, Direction d) {return getAdjacentSquare(s.getSquareX(), s.getSquareY(), d);}
	public BoardSquare getAdjacentSquare(int x, int y, Direction d)
	{
		try{
			switch(d)
			{
				case LEFT_UP: return squares[x-1][y-1];
				case UP: return squares[x][y-1];
				case RIGHT_UP: return squares[x+1][y-1];
				case LEFT: return squares[x-1][y];
				case RIGHT: return squares[x+1][y];
				case LEFT_DOWN: return squares[x-1][y+1];
				case DOWN: return squares[x][y+1];
				case RIGHT_DOWN: return squares[x+1][y+1];
				default: return null;
			}
		}
		catch(ArrayIndexOutOfBoundsException e) {return null;}
	}

	public void placeAll(Piece[][] p)
	{
		for(Piece[] pBox : p)
			for(Piece p1 : pBox)
				if(p1 != null) squares[p1.getX()][p1.getY()].setPiece(p1);
	}
	public void place(int x, int y, Piece p)
	{
		if(squares[x][y].hasPiece()) Chess.capture(squares[x][y].getPiece());
		squares[x][y].setPiece(p);
	}
	public void tempPlace(int x, int y, Piece p) {squares[x][y].tempPiece(p);}
	
	public void selectedSquare(BoardSquare s) {selected = s;}

	public BoardSquare getSelectedSquare() {return selected;}

	public void highlightSquares(ArrayList<BoardSquare> squares)
	{
		for(BoardSquare s : squares)
			s.highlight();
	}

	public void clearHighlights()
	{
		for(BoardSquare[] sBox : squares)
			for(BoardSquare s : sBox)
				s.clearHighlight();
	}

	public enum Direction {LEFT_UP, UP, RIGHT_UP, LEFT, RIGHT, LEFT_DOWN, DOWN, RIGHT_DOWN;}
}