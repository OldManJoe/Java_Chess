package chess;

import java.util.ArrayList;

import javax.swing.ImageIcon;

public class King extends Piece 
{
	private static final int QUEEN_ROOK = 8, KING_ROOK = 15, WHITE_ROW = 7, BLACK_ROW = 0;
	private boolean canCastle = true;

	public King(int x, int y, int s) 
	{
		super(x, y, s);
		setImage(new ImageIcon((s == WHITE) ? "WKing.png" : "BKing.png"));
	}

	public void move(int x, int y)
	{
		int row = (getSide() == WHITE) ? WHITE_ROW : BLACK_ROW;
		if(Math.abs(getX() - x) == 2) castle(getX() - x, row);

		super.move(x, y);
		canCastle = false;
	}

	public boolean canMove(int x, int y)
	{
		int move = getX() - x, row = (getSide() == WHITE) ? WHITE_ROW : BLACK_ROW;
		Rook kingRook = (Rook)Chess.getPiece(getSide(), KING_ROOK), queenRook = (Rook)Chess.getPiece(getSide(), QUEEN_ROOK);
		boolean clear = true;

		//castling
		if(Math.abs(move) == 2 && canCastle && !Chess.testForCheck(x, y) && !Chess.testForCheck(x + move, y) && !Chess.testForCheck(x + move/2, y))
			if(move < 0 && kingRook != null && kingRook.canCastle())
			{
				for(int a=5;a<7;a++)
					if(getBoard().getSquare(a, row).hasPiece()) {clear = false; break;}

				return clear;
			}
			else if(move > 0 && queenRook != null && queenRook.canCastle())
			{
				for(int a=1;a<4;a++)
					if(getBoard().getSquare(a, row).hasPiece()) {clear = false; break;}

				return clear;
			}
			else return false;

		if(super.canMove(x, y)) return true;
		else return false;
	}

	public boolean canAttack(int x, int y)
	{
		if(Math.abs(getX() - x) < 2 && Math.abs(getY() - y) < 2) return true;
		else return false;
	}

	public ArrayList<BoardSquare> getMoves() 
	{
		ArrayList<BoardSquare> squares = new ArrayList<BoardSquare>();
		BoardSquare s;
		int row = (getSide() == WHITE) ? WHITE_ROW : BLACK_ROW;
		
		for(Board.Direction d : Board.Direction.values())
		{
			s = getBoard().getAdjacentSquare(getX(), getY(), d);
			if(s != null && canMove(s.getSquareX(), s.getSquareY())) squares.add(s);
		}
		
		if(canCastle)
		{
			if(canMove(2, row)) squares.add(getBoard().getSquare(2, row));
			if(canMove(6, row)) squares.add(getBoard().getSquare(6, row));
		}
		
		return squares;
	}

	private void castle(int move, int row)
	{
		if(move < 0)
			((Rook)Chess.getPiece(getSide(), KING_ROOK)).move(5, row);
		else if(move > 0)
			((Rook)Chess.getPiece(getSide(), QUEEN_ROOK)).move(3, row);
	}

	public String toString()
	{
		if(getSide() == WHITE) return "WKing " + getX() + " " + getY();
		else return "BKing " + getX() + " " + getY();
	}
}