package chess;

import java.util.ArrayList;
import javax.swing.ImageIcon;

public abstract class Piece
{
	public static final int WHITE = 0, BLACK = 1;
	
	private static Board board;

	private int x, y, side;
	private ImageIcon image;

	public Piece(int x1, int y1, int s) {x=x1; y=y1; side=s;}

	public static void setBoard(Board b) {board=b;}

	public void move(int x1, int y1)
	{
		board.clear(x,y);
		x=x1; y=y1;
		board.place(x1,y1,this);
	}
	public void tempMove(int x1, int y1)
	{
		x=x1; y=y1;
	}
	
	public boolean canMove(int x, int y)
	{
		Piece p = getBoard().getSquare(x, y).getPiece();
		return (canAttack(x,y) && (p == null || p.getSide() != getSide()) && Chess.testToBlockCheck(x, y, this));
	}

	protected ArrayList<BoardSquare> testForCheck(ArrayList<BoardSquare> s)
	{
		ArrayList<BoardSquare> squares = s;
		
		for(int a=squares.size()-1;a>-1;a--)
			if(!Chess.testToBlockCheck(squares.get(a).getSquareX(), squares.get(a).getSquareY(), this))
				squares.remove(a);
		
		return squares;
	}
	
	public int getX() {return x;}
	public int getY() {return y;}
	public int getSide() {return side;}
	public void setImage(ImageIcon i) {image = i;}
	public ImageIcon getImage() {return image;}
	public Board getBoard() {return board;}
	
	public abstract String toString();
	public abstract boolean canAttack(int x, int y);
	public abstract ArrayList<BoardSquare> getMoves();
}