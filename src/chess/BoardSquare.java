package chess;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class BoardSquare extends JLabel
{
	private static final Color WHITE = Color.LIGHT_GRAY;
	private static final Color WHITE_HIGHLIGHT = Color.YELLOW;
	private static final Color BLACK = Color.DARK_GRAY;
	private static final Color BLACK_HIGHLIGHT = new Color(234, 234, 0);
	private static final Color RED = Color.RED;
	private static final Color BLUE = Color.BLUE;
	
	private static Board board;

	private int x, y;
	private Piece piece = null;

	public BoardSquare(int x1, int y1, MouseListener ml)
	{
		setPreferredSize(new Dimension(60, 60));
		setOpaque(true);
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		x=x1;y=y1;
		addMouseListener(ml);
		clearHighlight();
	}

	public static void setBoard(Board b) {board = b;}
	
	public void onClick()
	{
		if(board.getSelectedSquare() == null)
			highlightMoves();
		else
		{
			Piece selectedPiece = board.getSelectedSquare().getPiece();

			if(selectedPiece.canMove(x,y))
			{
				Chess.setLastPawn(null);
				selectedPiece.move(x,y);
				board.clearHighlights();
				board.selectedSquare(null);
				Chess.toggleTurn();
			}
			else
			{
				board.clearHighlights();
				highlightMoves();
			}
		}
	}

	public boolean hasPiece() {return piece != null;}
	public Piece getPiece() {return piece;}
	public void setPiece(Piece p) 
	{
		piece = p;
		
		if(p != null) setIcon(p.getImage());
		else setIcon(null);
	}
	public void tempPiece(Piece p) {piece = p;}
	
	public void highlight()
	{
		Piece piece = board.getSelectedSquare().getPiece();
		
		if(piece != null && piece.getClass() == Pawn.class && (y == 0 || y == 7)) setBackground(BLUE); 
		else if(hasPiece() || (piece.getClass() == Pawn.class && x != piece.getX())) setBackground(RED);
		else setBackground((getBackground() == WHITE) ? WHITE_HIGHLIGHT : BLACK_HIGHLIGHT);
	}

	private void highlightMoves()
	{
		if(piece != null && ((piece.getSide() == Piece.WHITE && Chess.isWhiteTurn()) || (piece.getSide() == Piece.BLACK && !Chess.isWhiteTurn())))
		{
			board.selectedSquare(this);
			board.highlightSquares(piece.getMoves());
		}
		else board.selectedSquare(null);
	}
	
	public void clearHighlight()
	{
		if(x % 2 == y % 2) setBackground(WHITE);
		else setBackground(BLACK);
	}
	
	public int getSquareX() {return x;}
	public int getSquareY() {return y;}
}