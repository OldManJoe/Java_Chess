package chess;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

@SuppressWarnings("serial")
public class Chess extends JPanel implements MouseListener
{
	private static Chess contentPane = new Chess();
	private static Board board;
	//0=white, 1=black, 0-7 pawns, 8,15=rooks, 9,14=knights, 10,13=bishops, 11=queen, 12=king
	private static final int WHITE=0, BLACK=1, QR=8, KR=15, QN=9, KN=14, QB=10, KB=13, Q=11, K=12;
	private static Piece[][] pieces = new Piece[2][16];
	private static Pawn lastPawn = null;
	private static boolean isWhiteTurn;

	public static void main(String[] args)
	{
		//Schedule a job for the event dispatch thread:
		//creating and showing this application's GUI
		SwingUtilities.invokeLater(
			new Runnable() 
			{
				public void run()
				{
					//Turn off metal's use of bold fonts
					UIManager.put("swing.boldMetal", Boolean.FALSE);
					createAndShowGUI();
				}
			});

		board = new Board(contentPane);
		setupGame();
		Piece.setBoard(board);
		BoardSquare.setBoard(board);
	}

	private static void createAndShowGUI()
	{
		//Create and set up the window.
		JFrame frame = new JFrame("Chess");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Add content to the window.
		contentPane.setLayout(new GridLayout(8,  8));
		frame.add(contentPane);
		
		for(int b=0;b<8;b++)
			for(int a=0;a<8;a++)
				contentPane.add(board.displaySquares()[a][b]);
		
		//Display the window
		//frame.setSize(300, 150);
		frame.pack();
		frame.setVisible(true);
	}
	
	public void mouseClicked(MouseEvent e) {((BoardSquare)e.getComponent()).onClick();}
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}

	private static void setupGame()
	{
		isWhiteTurn = true;
		
		//setupPieces
		for(int s=0;s<2;s++)
			for(int p=0;p<16;p++)
				addPiece(s,p);
		
		//setup board
		board.clear();
		board.placeAll(pieces);
	}

	private static void addPiece(int s, int p)
	{
		int y = (s == Piece.WHITE) ? 7 : 1;
		
		//0-7 pawns, 8,15=rooks, 9,14=knights, 10,13=bishops, 11=queen, 12=king
		if(p < 8) pieces[s][p] = new Pawn(p,y-1+s,s);
		else if(p == QR || p == KR) pieces[s][p] = new Rook(p-8,y-s,s);
		else if(p == QN || p == KN) pieces[s][p] = new Knight(p-8,y-s,s);
		else if(p == QB || p == KB) pieces[s][p] = new Bishop(p-8,y-s,s);
		else if(p == Q) pieces[s][p] = new Queen(p-8,y-s,s);
		else if(p == K) pieces[s][p] = new King(p-8,y-s,s);
	}
	public static Piece getPiece(int s, int p) {return pieces[s][p];}

	public static King getKing()
	{
		int s = (isWhiteTurn) ? 0 : 1;
		return (King)pieces[s][K];
	}
	
	public static void setLastPawn(Pawn p) {lastPawn = p;}
	public static Pawn getLastPawn() {return lastPawn;}
	
	public static void capture(Piece p)
	{
		for(int s=0;s<2;s++)
			for(int p1=0;p1<16;p1++)
				if(pieces[s][p1] == p)
				{
					pieces[s][p1] = null;
					return;
				}
	}

	public static boolean testForCheck(int x, int y)
	{
		int attackingSide = (isWhiteTurn) ? BLACK : WHITE;

		for(Piece p : pieces[attackingSide]) if(p != null && p.canAttack(x,y)) return true;
		
		return false;
	}	
	public static boolean testForCheckMate()
	{
		int defendingSide = (isWhiteTurn) ? WHITE : BLACK;

		for(Piece p : pieces[defendingSide]) if(p != null && p.getMoves().size() != 0) return false;
		
		return true;
	}
	public static boolean testToBlockCheck(int x, int y, Piece p)
	{
		int s = -1, p1 = -1, tempX = p.getX(), tempY = p.getY();
		Piece tempCapture = board.getSquare(x, y).getPiece(), originalPiece = board.getSquare(tempX, tempY).getPiece();
		
		if(tempCapture != null)
		{
			s = tempCapture.getSide();
			for(int a=0;a<16;a++) if(pieces[s][a] == tempCapture) p1 = a;
		}
		
		p.tempMove(x, y);
		board.tempClear(tempX, tempY);
		board.tempPlace(x, y, p);
		if(tempCapture != null) capture(tempCapture);
		
		boolean blocked = !testForCheck(getKing().getX(), getKing().getY());
		
		p.tempMove(tempX, tempY);
		board.tempPlace(tempX, tempY, originalPiece);
		board.tempPlace(x, y, tempCapture);
		if(p1 >= 0) pieces[s][p1] = tempCapture;
		
		return blocked;
	}
	
	public static boolean isWhiteTurn() {return isWhiteTurn;}

	public static void promote(Pawn p)
	{
		int s = p.getSide(), i = -1;
		Piece promotion = new Queen(p.getX(), p.getY(), p.getSide());
		
		for(int a=0;a<8;a++)
			if(pieces[s][a] == p) i = a;

		Object[] possibilities = {"Queen", "Rook", "Bishop", "Knight"};
		String n = (String)JOptionPane.showInputDialog(contentPane, "Choose promotion", "Pawn Promotion", JOptionPane.PLAIN_MESSAGE, null, possibilities, "Queen");
		
		switch(n){
		case "Queen": break;
		case "Rook": promotion = new Rook(p.getX(), p.getY(), p.getSide()); break;
		case "Bishop": promotion = new Bishop(p.getX(), p.getY(), p.getSide()); break;
		case "Knight": promotion = new Knight(p.getX(), p.getY(), p.getSide()); break;
		}
		pieces[s][i] = promotion;
		board.getSquare(promotion.getX(), promotion.getY()).setPiece(promotion);
	}

	public static void toggleTurn()
	{
		isWhiteTurn = !isWhiteTurn;
		
		if(testForCheck(getKing().getX(), getKing().getY()) && testForCheckMate()) 
		{
			String winner = (!isWhiteTurn) ? "White" : "Black";
			int n = JOptionPane.showConfirmDialog(contentPane, winner + " wins! Would you like to play again?", "Play Again?", JOptionPane.YES_NO_OPTION);
			
			if(n == JOptionPane.YES_OPTION) setupGame();
			else System.exit(0);
		}
	}
}